# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Merle Carole carole.merle.etu@univ-lille.fr 

- Nom, Prénom, email: Aggoune dounia
## Question 1


![Screenshot](screen/creationUtilisateurTOTO.png)

<img src="screen/addingtotoTOubuntu.png">  

Toto n'a pas les droit en ecriture sur le fichier myfile.txt donc un processus lancé par toto ne pourra pas ecrire dans ce fichier.

## Question 2  
**Que signifie le caractère x pour un répertoire ?**  
Pour un répertoire ça signifie qu'on a accès aux sous-répertoires dans le répertoire. 

<img src="screen/qst02Part01.png">  
  Si on enlève le droit d'execution au répertoire mydir au groupe ubuntu, toto ne pourra pas accéder à ce répertoire étant donné qu'il est dans le groupe ubuntu.  

<img src="screen/qst02Part02.png">  
 
  L'utilisateur toto n'a pas accès au répertoire mydir et n'a donc pas accès à tout ce qu'il contient.

## Question 3  

On execute le programme avec l'utilisateur toto :  
<img src="screen/Q3-1.png">  

Après activation du flag set-user-id du fichier exécutable :  
<img src="screen/Q3-2.png">  


## Question 4

Les fonctions  getguid() geteuid() getid()
permettent d'obtenir facilement les id du processus en cours.

## Question 5  
 
À quoi sert la commande chfn ?   

**chfn** sert à modifier les information d'utilisateurs (infos type nom d'un utilisateur, numéro de bureau, numéro, de téléphone professionnel...).  

Donnez les résultats de ls -al /usr/bin/chfn, et expliquez les permissions.  
<img src="screen/usr_bin_chfn.png">  
Au niveau des permissions, l'utilisateur root peut lire, modifier et executer. On retrouve le set-user-id défini pour ce fichier. Quand la command est exécutée son effective uid est égal à celui de root (car propriétire du fichier), pour les autres groupes et utilisateurs ils peuvent lire et executer. 

cat etc/passwd avant que toto run chfn :
<img src="screen/cat-passwd-avt.png">  

cat etc/passwd après que toto run chfn :
<img src="screen/cat-passwd-apres.png">  

## Question 6

Les mots de passe sont stockés dans etc/shadow car seulement root peut lire, les autres utilisateurs ne peuvent pas le lire. Si on mettait les mots de passes dans etc/passwd n'importe quel utilisateur pourrait les lire. 

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

commands:  
adduser lambda_a  
adduser lambda_b  
addgroup groupe_a  
addgroup groupe_b  
adduser admin  
adduser lambda_a groupe_a  
adduser lambda_b groupe_b  
adduser admin_server_fichier groupe_b  
adduser admin_server_fichier groupe_a    
su admin  
admin$ mkdir dir_a dir_b dir_c  
admin$ chgrp dir_a groupe_a  
admin$ chgrp dir_b groupe_b  
admin$ chmod g+s dir_a  
admin$ chmod g+s dir_b  
admin$ chmod o-r dir_a  
admin$ chmod o-r dir_a  
admin$ chmod +t dir_a  
admin$ chmod +t dir_b  

chmod g+s dir_a/dir_b pour que les sous-répertoires et fichiers créés dans les dir_a et dir_b appartiennent au même groupe groupe_a pour les sous-répertoires de dir_a et groupe_b pour ceux de dir_b.

chmod +t pour dir_a et dir_b pour que les utilisateurs ne puissent pas supprimer ou renommer des fichiers dont ils ne sont pas propriétaires tout en pouvant quand même lire, modifier ou créer des fichier dans ces répertoires. 

**Il est possible (nécessaire ?) de créer d’autres groupes et utilisateurs.**

Nous avons également créé deux autres utilisateurs (lambda_aa et lambda_bb) pour tester qu'un utilisateur ne puisse pas supprimer ou renommer un fichier qui ne lui appartient pas.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

Le programme sera utilisable par tous les utilisateurs du groupe_a ou du groupe_b:

su admin
cd /home/admin/
touch passwd
chmod g-w passwd
chmod o-w passwd
chmod g-r passwd
chmod o-r passwd

test :

su lambda_a 
cd /home/dir_a
makdir testQ8
cd testQ8
touch Q8

su lambda_aa

cd to where is rmg

./rmg /home/dir_a/testQ8/Q8

## Question 9

Le programme et les scripts dans le repertoire *question9*.
 
chmod o-wx pwg  
chmod g-w pwg  
chmod g+x pwg 

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








