#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <pwd.h>
#include <unistd.h>
#include <errno.h>
#include <grp.h>
#include <string.h> 
#include <sys/stat.h>
#include "check_pass.h"


int checkFileExistence(const char* filepath) {

	int fd = access(filepath, F_OK); 
    if(fd == -1){ 
        printf("Error Number : %d\n", errno); 
        perror("Error Description:"); 
        return -1;
    } else {
    	printf("file exists\n");
    	return 0;
    }
}

int checkReadAndWritePermissions(const char* filepath) {

	int fd = access(filepath, R_OK & W_OK); 
    if(fd == -1){ 
        printf("Error Number : %d\n", errno); 
        perror("Error Description:"); 
        return -1;
    } 
    
    printf("read and write permissions ok\n");
    return 0; 
}

int belongToCorrectgroup(__uid_t uid, const char* filepath) {

    //get group of file
	struct stat sb;
    stat(filepath, &sb);
    struct group *gr_file = getgrgid(sb.st_gid);
    if(gr_file == NULL) {
    	perror("getgrgid error: \n");
        return -1;
    } 
    
    //printf("the file belongs to group %s\n", gr_file->gr_name);

    struct passwd* pw = getpwuid(uid);
    if(pw == NULL){
        perror("getpwuid error: \n");
        return -1;
    }

    int ngroups = 0;

    //get the number of groups of user
    getgrouplist(pw->pw_name, pw->pw_gid, NULL, &ngroups);
    __gid_t groups[ngroups];

    //get the group(s) of user
    getgrouplist(pw->pw_name, pw->pw_gid, groups, &ngroups);

    //printf("user belongs to %d groups\n", ngroups);

    //compare file group and user group to check if user belongs to the group of the file
    for (int i = 0; i < ngroups; i++){
        struct group* gr = getgrgid(groups[i]);
        if(gr == NULL){
            perror("getgrgid error: \n");
            return -1;
        } 

        if(strcmp(gr->gr_name, gr_file->gr_name) == 0) {
            printf("user and file belong to same group\n");
            return 0;
        }
    }
    return -1;
}


int rmg(const char *filepath, const char *user, const char *password) {

    if(checkFileExistence(filepath) == 0) { //vérifie que le fichier en argument existe bien
    	if((checkReadAndWritePermissions(filepath) == 0)) { // vérifie que le user a bien les droit sur le file
    		if(belongToCorrectgroup(getuid(), filepath) == 0) { //vérifie that user belongs to either groupe_a or groupe_b
    			if (checkPass(user, password) == 0) { //vérifie mot de passe 
		            if(remove(filepath) == 0) {
		                printf("The file was deleted successfully\n");
		                return 0;
		            } else {
		                perror("There was an error, the file could not be deleted.\n");
		                return -1;
		            }
		        } else {
		            printf("Invalid password\n");
		            return -1;
		        } 
    		} else {
    			printf("Incorrect group\n");
		        return -1;
    		}
    	} else {
    		printf("Incorrect file permissions\n");
		    return -1;
    	}
    } else {
    	printf("Incorrect file name or file path\n");
		return -1;
    }
    return -1;   
}


int main(int argc, char *argv[]) {

    if(argc < 1) {
        printf("You must add a file to delete\n");
        exit(0);
    }

    //get user
    uid_t uid = getuid();
    struct passwd* pw = getpwuid(uid);
    if (pw) {
      puts(pw->pw_name);
    } else {
        fprintf(stderr,"Cannot find username for UID %u\n", (unsigned) uid);
        exit(1);
    }

    //user input password
    char mdp[60]; 
    printf("Enter your password \n");
    scanf("%s", mdp);

    if(rmg(argv[1], pw->pw_name, mdp) == -1) {
    	printf("the file was not Deleted\n");
    }
    return 0;
}
