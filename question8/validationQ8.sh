#!/bin/bash
set -x

su - admin_server_fichier <<!
adminserverfichier
cd /home/admin_server_fichier/
make clean
make
!

su - lambda_a <<!
lambda_a

echo -n "Test with user :"; whoami

echo "lambda_a creates a dir called testQ8_1 in dir_a and a file called Q8_1 in dir testQ8_1" 
cd /home/dir_a/
mkdir testQ8_1
cd testQ8_1
touch Q8_1

cd /home/admin_server_fichier/

echo "then we start rmg with the command: ./rmg /home/dir_a/testQ8_1/Q8_1" 
./rmg /home/dir_a/testQ8_1/Q8_1

echo "then we go to /home/dir_a/testQ8_1/" 
cd /home/dir_a/testQ8_1
echo "we test with ls to check that file Q8 has been deleted:" 
ls
!

