#include "check_pass.h"
#include <pwd.h>
#include <string.h> 
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <crypt.h>

/// @return 0 - password is correct, otherwise no
int checkPass(const char* user, const char* password) {

    FILE * fp = fopen("/home/admin_server_fichier/passwd", "r"); 
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    if (fp == NULL) {
		perror("Cannot check password");
        exit(1);
	}

    while((read = getline(&line, &len, fp)) != -1) {
    	// printf("current line = %s\n", line); 
    	char* token_1 = strtok(line, ":"); 
    	// printf("current token_1 = %s\n", token_1); 
        char* token_2 = strtok(NULL, ":"); 
    	// printf(" token_2 = %s\n", token_2); 

        if(strcmp(user, token_1)) {
        	//check password
            // TODO: decrypt password 
        	if(strcmp(password, token_2)){
        		return 0;
        	} 
        }
    }

    fclose(fp);

    if (line) {
        free(line);
    }

    return -1;
}