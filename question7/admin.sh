#!/bin/bash

#prepare environement de test:
su - lambda_a <<!
lambda_a

cd /home/dir_a 
touch test.txt
!

su - lambda_b <<!
lambda_bpassword

cd /home/dir_b
touch test.txt
!

su - admin_server_fichier <<!
adminserverfichier
#end

#start tests
echo "************** STARTING TESTS **************"
echo -n "Test with user :"; whoami 

echo "--------------------------------------------------------------"

echo "Test 1:  création et modification de fichiers dans dir_c"
cd /home/dir_c
echo -n "pwd = "; pwd
echo "cmd de test : touch myfile.txt"
touch myfile.txt
echo "cmd de test : mkdir dir"
mkdir dir
echo "cmd de test : ls -la"
ls -la
echo "cmd de test : echo \"Hello world\" > test.txt"
echo "Hello world" > test.txt
echo "cmd de test : cat test.txt"
cat test.txt

echo "--------------------------------------------------------------"

echo "Test 2: il peut effacer (ou renommer) des fichiers dans dir_a, dir_b et dir_c"
cd /home/dir_a
echo -n "pwd = "; pwd
echo "cmd de test : ls -la"
ls -la
echo "cmd de test : mv test.txt  myTest.txt"
mv test.txt  myTest.txt
echo "cmd de test : ls -la"
ls -la
echo "cmd de test : rm -r *"
rm -r *
echo "cmd de test : ls -la"
ls -la

echo "--------------------------------------------------------------"

cd /home/dir_b
echo -n "pwd = "; pwd
echo "cmd de test : ls -la"
ls -la
echo "cmd de test : mv test.txt  myTest.txt"
mv test.txt  myTest.txt
echo "cmd de test : ls -la"
ls -la
echo "cmd de test : rm -r *"
rm -r *
echo "cmd de test : ls -la"
ls -la

echo "--------------------------------------------------------------"

cd /home/dir_c
echo -n "pwd = "; pwd
echo "cmd de test : ls -la"
ls -la
echo "cmd de test : mv test.txt  myTest.txt"
mv test.txt  myTest.txt
echo "cmd de test : ls -la"
ls -la
echo "cmd de test : rm -r *"
rm -r *
echo "cmd de test : ls -la"
ls -la
!






