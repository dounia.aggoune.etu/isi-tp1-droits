#include<stdio.h>
#include<string.h>  
#include<sys/socket.h>
#include<arpa/inet.h> 
#include<unistd.h>
#include<pwd.h>
#include<sys/types.h>
#include<dirent.h>
#include<stdlib.h>
// #include <crypt.h>

/// @return 0 - password is correct, otherwise no
int checkPass(const char* user, const char* password);

// void childProcess() {

// 	while((read_size = recv(client_sock , client_message , 2000 , 0)) > 0) {
		
// 		if(recv(sock , client_reply , 2000 , 0) < 0) {
// 		  puts("recv failed");
// 		  break;
// 		}

// 		char* cmd = strtok(client_reply, " "); 
// 		char* arg = strtok(NULL, " ");

// 		if(strcmp(cmd, "list")) {
// 			if(strcmp(arg, "dir_a") || strcmp(arg, "dir_b") || strcmp(arg, "dir_c")) {
// 				//ls arg


// 				struct dirent **namelist; 
// 				int n = scandir(arg, &namelist, NULL, alphasort);  
// 				if(n < 0) { 
// 					perror("cannot list directory files"); 
// 					exit(EXIT_FAILURE); 
// 				} else { 
// 					while (n–) { 
// 						printf("%s\n", namelist[n]->d_name); 
// 						free(namelist[n]); 
// 					} 
// 					free(namelist); 
// 				} 
// 			} else {
// 				printf("Cannot list directory: incorrect directory%s\n", );
// 			}
// 		} else if(strcmp(cmd, "read")) {
// 			//cat arg
// 			FILE * fp = fopen(arg, "r");
// 	        char * line = NULL;
// 	        size_t len = 0;
// 	        ssize_t read;

// 	        if (fp == NULL) {
// 	            perror("Cannot read file");
// 	            exit(EXIT_FAILURE);
// 	        }

// 	        while((read = getline(&line, &len, fp)) != -1) {
// 	            printf("%s\n", line);
// 	        }

// 	        fclose(fp); 

// 	        if (line) {
// 	            free(line);
// 	        }
// 		} else if(strcmp(cmd, "close")) {
// 			//quit process
// 			exit(EXIT_SUCCESS);

// 		}
// 	}
// }


int main(int argc , char *argv[]) {

	int socket_desc , client_sock , c , read_size;
	struct sockaddr_in server , client;
	char client_message[2000], client_reply[2000];
  
	//Create socket
	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
	if (socket_desc == -1) {
		printf("Could not create socket");
	}
	puts("Socket created");
  
	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(4000);
  
	if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0) {
		perror("bind failed. Error");
		return 1;
	}
	puts("bind done");
  
	listen(socket_desc , 3);
  
	//Accept and incoming connection
	puts("Waiting for incoming connections...");
	c = sizeof(struct sockaddr_in);
  
	//accept connection from an incoming client
	client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
	if (client_sock < 0) {
		perror("accept failed");
		return 1;
	}
	puts("Connection accepted");
  
	//Receive a message from client
	while((read_size = recv(client_sock, client_message, 2000, 0)) > 0) {
		//first we get username and password of user 
		if(recv(client_sock, client_reply, 2000, 0) < 0) {
	  		puts("recv failed");
	  		break;
		}

		//first line received contains username and password
		char* user = strtok(client_reply, " "); 
		char* password = strtok(NULL, " "); 

		//we verify password
		if(checkPass(user, password) == 0) {
		    int id = fork();
		    //Le processus fils incarne l’utilisateur qui vient de se connecter en 
		    //changeant son EUID et EGID avant d’intéragir avec le client
			if(id == 0) {
				//we need to get euid and egid of user
				//first we search user in passwd
				struct passwd* entry = getpwent();
			    if(entry == NULL) {
			        perror("Error reading password database\n");
		            exit(EXIT_FAILURE);
		    	}

				if(entry->pw_name == user) {
			    	//we set EUID & EGID
			    	seteuid(entry->pw_uid);
			    	setegid(entry->pw_uid);
				} else {
			    	perror("Error: username not found\n");
			        exit(EXIT_FAILURE);
			    }
				
				endpwent();




				// childProcess();



		char* cmd = strtok(client_reply, " "); 
		char* arg = strtok(NULL, " ");

		if(strcmp(cmd, "list")) {
			if(strcmp(arg, "dir_a") || strcmp(arg, "dir_b") || strcmp(arg, "dir_c")) {
				//ls arg


				struct dirent **namelist; 
				int n = scandir(arg, &namelist, NULL, alphasort);  
				if(n < 0) { 
					perror("cannot list directory files"); 
					exit(EXIT_FAILURE); 
				} else { 
					while(n > 0) { 
						printf("%s\n", namelist[n]->d_name); 
						free(namelist[n]); 
						n--;
					} 
					free(namelist); 
				} 
			} else {
				printf("Cannot list directory: incorrect directory\n");
			}
		} else if(strcmp(cmd, "read")) {
			//cat arg
			FILE * fp = fopen(arg, "r");
	        char * line = NULL;
	        size_t len = 0;
	        ssize_t read;

	        if (fp == NULL) {
	            perror("Cannot read file");
	            exit(EXIT_FAILURE);
	        }

	        while((read = getline(&line, &len, fp)) != -1) {
	            printf("%s\n", line);
	        }

	        fclose(fp); 

	        if (line) {
	            free(line);
	        }
		} else if(strcmp(cmd, "close")) {
			//quit process
			exit(EXIT_SUCCESS);

		}






			} else if(id > 0) { 
			// Le processus père ferme la connexion avec le client et se remet en écoute d’une prochaine connexion.
				close(client_sock);
		  	}
		}
	}
  
	if(read_size == 0) {
		puts("Client disconnected");
		fflush(stdout);
	} else if(read_size == -1) {
		perror("recv failed");
	}
  
	return 0;
}

/// @return 0 - password is correct, otherwise no
int checkPass(const char* user, const char* password) {

	FILE * fp = fopen("/home/admin_server_fichier/passwd", "r"); 
	char * line = NULL;
	size_t len = 0;
	ssize_t read;

	if (fp == NULL) {
		perror("Cannot check password");
		exit(EXIT_FAILURE);
  	}

	while((read = getline(&line, &len, fp)) != -1) {
	  char* token_1 = strtok(line, " "); 
		char* token_2 = strtok(NULL, " "); 

		if(strcmp(user, token_1)) {
			//check password
			if(strcmp(password, token_2)) {
				return 0;
		    } 
		}
	}

	fclose(fp);

	if (line) {
		free(line);
	}

	return -1;
}
