#include <stdio.h>
#include <grp.h>
#include <stdlib.h>
#include <pwd.h>
#include <unistd.h>
#include <string.h>
#include "check_pass.h"


int belongToCorrectgroup(__uid_t uid) {
	struct passwd* pw = getpwuid(uid);
	if(pw == NULL){
	    perror("getpwuid error: ");
	}

	int ngroups = 0;

	//this call is just to get the correct ngroups
	getgrouplist(pw->pw_name, pw->pw_gid, NULL, &ngroups);
	__gid_t groups[ngroups];

	//here we actually get the groups
	getgrouplist(pw->pw_name, pw->pw_gid, groups, &ngroups);


	//example to print the groups name
	for (int i = 0; i < ngroups; i++){
	    struct group* gr = getgrgid(groups[i]);
	    if(gr == NULL){
	        perror("getgrgid error: ");
	    }
	    if((strcmp(gr->gr_name, "groupe_a") == 0) || (strcmp(gr->gr_name, "groupe_b") == 0)) {
	    	return 0;
	    }
	    //printf("%s\n",gr->gr_name);
	}
	return -1;
}

char* confirmPassword(char * newPass) {
	char pwd1[60], pwd2[60]; 

	while(1) {
		printf("Enter new password\n");
		scanf("%s", pwd1);
	   	printf("Confirm new password\n");
		scanf("%s", pwd2);
		if(strcmp(pwd1, pwd2) == 0) {
			newPass = pwd1;
			return newPass;
		} else {
			printf("passwords don't match, try again\n");
		}
	}
	newPass = pwd1;
	return newPass;
}

int main() {

	// verify user belongs to group a or b
	__uid_t uid = getuid();

	char mdp[60]; 

	if(belongToCorrectgroup(uid) == 0) {
	
		//get user
	    uid_t uid = getuid();
	    struct passwd* pw = getpwuid(uid);
	    if (pw) {
	      // puts(pw->pw_name);
	    } else {
	        fprintf(stderr,"Cannot find username for UID %u\n", (unsigned) uid);
	        exit(1);
	    }

	    //verify if user already has a password in admin/passwd
	    if(hasPassword(pw->pw_name) == 0) { //user already has a password
	    	//user input password
		    printf("Enter your password \n");
		    scanf("%s", mdp);
		    if(checkPass(pw->pw_name, mdp) == 0) { //password is correct
		    	// newMotDePasse = confirmPassword(newP);

		    	char pwd1[60], pwd2[60]; 

				while(1) {
					printf("Enter new password\n");
					scanf("%s", pwd1);
				   	printf("Confirm new password\n");
					scanf("%s", pwd2);
					if(strcmp(pwd1, pwd2) == 0) {
						if(newPassword(pw->pw_name, pwd1, 1) == 0) { //new password has been encrypted and written in passwd
					    	printf("Your new password was recorded successfully\n");
					    	exit(EXIT_SUCCESS);
					    } else {
							printf("Error: new password was not recorded\n");
							exit(EXIT_FAILURE);
					    }
					} else {
						printf("passwords don't match, try again\n");
					}
				}
			} else {
				printf("Incorrect password\n");
				return -1;
			}
	    } else { //user does not already have a password
	    	// newMotDePasse = confirmPassword(newP);
	    	char pwd1[60], pwd2[60]; 

			while(1) {
				printf("Enter new password\n");
				scanf("%s", pwd1);
			   	printf("Confirm new password\n");
				scanf("%s", pwd2);
				if(strcmp(pwd1, pwd2) == 0) {
					if(newPassword(pw->pw_name, pwd1, 0) == 0) {
						printf("Your new password was recorded successfully\n");
						exit(EXIT_SUCCESS);
					} else {
						printf("Error we could not record your new password\n");
						exit(EXIT_FAILURE);
				    }
				} else {
					printf("passwords don't match, try again\n");
				}
			}
	    }
	} else {
		printf("You cannot change your password, because you don't belong to the correct group.\n");
		exit(1);
	}
	return 0;     
}