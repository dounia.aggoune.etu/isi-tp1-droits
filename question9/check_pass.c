#include "check_pass.h"
#include <pwd.h>
#include <string.h> 
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <crypt.h>
#include <assert.h>

//returns 0 if user has a password and -1 if user does not have a password
int hasPassword(const char* user) {

	FILE * fp = fopen("/home/admin_server_fichier/passwd", "r"); 
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    if (fp == NULL) {
		perror("Cannot check password");
        exit(1);
	}

    while((read = getline(&line, &len, fp)) != -1) {
    	char* username = strtok(line, " "); 
        if(strcmp(user, username) == 0) {
			return 0;
        }
    }

    fclose(fp);

    if (line) {
        free(line);
    }

    return -1;
}


/// @return 0 - password is correct, otherwise no
int checkPass(const char* user, const char* password) {

    FILE * fp = fopen("/home/admin_server_fichier/passwd", "r"); 
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    if (fp == NULL) {
		perror("Cannot check password");
        exit(EXIT_FAILURE);
	}

    while((read = getline(&line, &len, fp)) != -1) {
    	char* username = strtok(line, " "); 
        char* password_to_check = strtok(NULL, " "); 

        if(strcmp(user, username)) {
        	//check password
        	if(strcmp(password_to_check, crypt(password, password_to_check))) {
        		return 0;
        	} 
        }
    }

    fclose(fp);

    if (line) {
        free(line);
    }

    return -1;
}

int newPassword(const char* user, const char* password, int hasPassword) {

	if(hasPassword == 1) { //if username is already in the file 

		FILE * fp = fopen("/home/admin_server_fichier/passwd", "r");  
		char * line = NULL;
		size_t len = 0;
		ssize_t read;

		FILE * fp_cp = fopen("/home/admin_server_fichier/passwd_copy", "a");  

		if (fp == NULL) {
			perror("error writing password");
	        exit(EXIT_FAILURE);
		}
		if (fp_cp == NULL) {
			perror("error writing password");
	        exit(EXIT_FAILURE);
		}

	    //copy lines in new file without the line with the old password
	    while((read = getline(&line, &len, fp)) != -1) {
	    	char* line1;
	    	line1 = malloc(sizeof(line));
	    	for(unsigned int i=0; i<strlen(line); i++) {
	    		line1[i] = line[i];
	    	}
	    	char* token = strtok(line, " "); 
	        if(strcmp(user, token) != 0) {
	        	fprintf(fp_cp, "%s", line1);
	        }
	        if (line1) {
        	free(line1);
    		}
	    }

	    int del = remove("/home/admin_server_fichier/passwd");
	   	if (del != 0) {
	    	perror("Error: could not delete file");
	        return -1;
	   	}

		int ret = rename("/home/admin_server_fichier/passwd_copy", "/home/admin_server_fichier/passwd");
	
	    if(ret != 0) {
	    	printf("Error: unable to rename the file");
	    	return -1;
	    }

	    fclose(fp); 
	    fclose(fp_cp); 

	    if (line) {
        	free(line);
    	}
	} 
	// append new line with username and encrypted pw at the end of the passwd file
	
	//encrypts new password
	char* newPassword = crypt(password, password);

	int size = sizeof(newPassword) + sizeof(user) + sizeof(" ");
	char* newLine;
	newLine = malloc(size);
	newLine[0] = '\0';
   	strcat(newLine, user);
    strcat(newLine, " ");
    strcat(newLine, newPassword);

	FILE *f = fopen("/home/admin_server_fichier/passwd", "a");  
		
	if (f == NULL) {
	    perror("Error open file");
	    exit(EXIT_FAILURE);
	}

    fprintf(f, "\n%s", newLine);
   	return 0;

    fclose(f); 

    free(newLine);
	return -1;	
}
