#ifndef CHECK_PASS_H
#define CHECK_PASS_H

#include <stddef.h>

void rand_str(char *dest, size_t length);

int hasPassword(const char* user);

int checkPass(const char* user, const char* password);

int newPassword(const char* user, const char* password, int hasPassword);

#endif