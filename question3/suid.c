#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
        FILE *f;
        int c;
        printf("My EUID is: %d, \n my EGID is: %d, \n my RUID: %d, \n and this is my RGID: %d\n", geteuid(), getegid(), getuid(), getgid());

        f = fopen("/home/mydir/data.txt", "r");
        if (f == NULL) {
                perror("Cannot open file");
                exit(EXIT_FAILURE);
        }
        printf("File opens correctly\n");
        while ((c = fgetc(f)) != EOF) {
              putchar(c);
            }
        fclose(f);
        exit(EXIT_SUCCESS);
}